/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : roiback
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 27/01/2020 17:52:39
*/


-- ----------------------------
-- Sequence structure for administrador_customuser_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."administrador_customuser_groups_id_seq";
CREATE SEQUENCE "public"."administrador_customuser_groups_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for administrador_customuser_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."administrador_customuser_id_seq";
CREATE SEQUENCE "public"."administrador_customuser_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for administrador_customuser_user_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."administrador_customuser_user_permissions_id_seq";
CREATE SEQUENCE "public"."administrador_customuser_user_permissions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_id_seq";
CREATE SEQUENCE "public"."auth_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_group_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_permissions_id_seq";
CREATE SEQUENCE "public"."auth_group_permissions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auth_permission_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_permission_id_seq";
CREATE SEQUENCE "public"."auth_permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for contacto_asunto_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contacto_asunto_id_seq";
CREATE SEQUENCE "public"."contacto_asunto_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for contacto_comentario_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contacto_comentario_id_seq";
CREATE SEQUENCE "public"."contacto_comentario_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for contacto_contacto_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contacto_contacto_id_seq";
CREATE SEQUENCE "public"."contacto_contacto_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for django_content_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_content_type_id_seq";
CREATE SEQUENCE "public"."django_content_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for django_migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_migrations_id_seq";
CREATE SEQUENCE "public"."django_migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for administrador_customuser
-- ----------------------------
DROP TABLE IF EXISTS "public"."administrador_customuser";
CREATE TABLE "public"."administrador_customuser" (
  "id" int4 NOT NULL DEFAULT nextval('administrador_customuser_id_seq'::regclass),
  "password" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "last_login" timestamptz(6),
  "is_superuser" bool NOT NULL,
  "first_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "is_staff" bool NOT NULL,
  "is_active" bool NOT NULL,
  "date_joined" timestamptz(6) NOT NULL,
  "email" varchar(254) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of administrador_customuser
-- ----------------------------
INSERT INTO "public"."administrador_customuser" VALUES (1, 'pbkdf2_sha256$150000$IZoDrOyhm1jF$oWrWoXxC8s+VTQVVi8vNjiuuyAi9Pez3KqmwBg9RAi8=', '2020-01-27 17:04:56.659592-05', 'f', 'Administrador', 'Administrador', 'f', 't', '2020-01-27 16:12:30.997504-05', 'administrador@gmail.com');

-- ----------------------------
-- Table structure for administrador_customuser_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."administrador_customuser_groups";
CREATE TABLE "public"."administrador_customuser_groups" (
  "id" int4 NOT NULL DEFAULT nextval('administrador_customuser_groups_id_seq'::regclass),
  "customuser_id" int4 NOT NULL,
  "group_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for administrador_customuser_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."administrador_customuser_user_permissions";
CREATE TABLE "public"."administrador_customuser_user_permissions" (
  "id" int4 NOT NULL DEFAULT nextval('administrador_customuser_user_permissions_id_seq'::regclass),
  "customuser_id" int4 NOT NULL,
  "permission_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group";
CREATE TABLE "public"."auth_group" (
  "id" int4 NOT NULL DEFAULT nextval('auth_group_id_seq'::regclass),
  "name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group_permissions";
CREATE TABLE "public"."auth_group_permissions" (
  "id" int4 NOT NULL DEFAULT nextval('auth_group_permissions_id_seq'::regclass),
  "group_id" int4 NOT NULL,
  "permission_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_permission";
CREATE TABLE "public"."auth_permission" (
  "id" int4 NOT NULL DEFAULT nextval('auth_permission_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "content_type_id" int4 NOT NULL,
  "codename" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "public"."auth_permission" VALUES (1, 'Can add permission', 1, 'add_permission');
INSERT INTO "public"."auth_permission" VALUES (2, 'Can change permission', 1, 'change_permission');
INSERT INTO "public"."auth_permission" VALUES (3, 'Can delete permission', 1, 'delete_permission');
INSERT INTO "public"."auth_permission" VALUES (4, 'Can view permission', 1, 'view_permission');
INSERT INTO "public"."auth_permission" VALUES (5, 'Can add group', 2, 'add_group');
INSERT INTO "public"."auth_permission" VALUES (6, 'Can change group', 2, 'change_group');
INSERT INTO "public"."auth_permission" VALUES (7, 'Can delete group', 2, 'delete_group');
INSERT INTO "public"."auth_permission" VALUES (8, 'Can view group', 2, 'view_group');
INSERT INTO "public"."auth_permission" VALUES (9, 'Can add content type', 3, 'add_contenttype');
INSERT INTO "public"."auth_permission" VALUES (10, 'Can change content type', 3, 'change_contenttype');
INSERT INTO "public"."auth_permission" VALUES (11, 'Can delete content type', 3, 'delete_contenttype');
INSERT INTO "public"."auth_permission" VALUES (12, 'Can view content type', 3, 'view_contenttype');
INSERT INTO "public"."auth_permission" VALUES (13, 'Can add session', 4, 'add_session');
INSERT INTO "public"."auth_permission" VALUES (14, 'Can change session', 4, 'change_session');
INSERT INTO "public"."auth_permission" VALUES (15, 'Can delete session', 4, 'delete_session');
INSERT INTO "public"."auth_permission" VALUES (16, 'Can view session', 4, 'view_session');
INSERT INTO "public"."auth_permission" VALUES (17, 'Can add contacto', 5, 'add_contacto');
INSERT INTO "public"."auth_permission" VALUES (18, 'Can change contacto', 5, 'change_contacto');
INSERT INTO "public"."auth_permission" VALUES (19, 'Can delete contacto', 5, 'delete_contacto');
INSERT INTO "public"."auth_permission" VALUES (20, 'Can view contacto', 5, 'view_contacto');
INSERT INTO "public"."auth_permission" VALUES (21, 'Can add comentario', 6, 'add_comentario');
INSERT INTO "public"."auth_permission" VALUES (22, 'Can change comentario', 6, 'change_comentario');
INSERT INTO "public"."auth_permission" VALUES (23, 'Can delete comentario', 6, 'delete_comentario');
INSERT INTO "public"."auth_permission" VALUES (24, 'Can view comentario', 6, 'view_comentario');
INSERT INTO "public"."auth_permission" VALUES (25, 'Can add asunto', 7, 'add_asunto');
INSERT INTO "public"."auth_permission" VALUES (26, 'Can change asunto', 7, 'change_asunto');
INSERT INTO "public"."auth_permission" VALUES (27, 'Can delete asunto', 7, 'delete_asunto');
INSERT INTO "public"."auth_permission" VALUES (28, 'Can view asunto', 7, 'view_asunto');
INSERT INTO "public"."auth_permission" VALUES (29, 'Can add user', 8, 'add_customuser');
INSERT INTO "public"."auth_permission" VALUES (30, 'Can change user', 8, 'change_customuser');
INSERT INTO "public"."auth_permission" VALUES (31, 'Can delete user', 8, 'delete_customuser');
INSERT INTO "public"."auth_permission" VALUES (32, 'Can view user', 8, 'view_customuser');

-- ----------------------------
-- Table structure for contacto_asunto
-- ----------------------------
DROP TABLE IF EXISTS "public"."contacto_asunto";
CREATE TABLE "public"."contacto_asunto" (
  "id" int4 NOT NULL DEFAULT nextval('contacto_asunto_id_seq'::regclass),
  "asunto" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of contacto_asunto
-- ----------------------------
INSERT INTO "public"."contacto_asunto" VALUES (1, 'inquietudes');

-- ----------------------------
-- Table structure for contacto_comentario
-- ----------------------------
DROP TABLE IF EXISTS "public"."contacto_comentario";
CREATE TABLE "public"."contacto_comentario" (
  "id" int4 NOT NULL DEFAULT nextval('contacto_comentario_id_seq'::regclass),
  "comentario" varchar(200) COLLATE "pg_catalog"."default",
  "contacto_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for contacto_contacto
-- ----------------------------
DROP TABLE IF EXISTS "public"."contacto_contacto";
CREATE TABLE "public"."contacto_contacto" (
  "id" int4 NOT NULL DEFAULT nextval('contacto_contacto_id_seq'::regclass),
  "nombre" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "apellido" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "fecha_nacimiento" date NOT NULL,
  "genero" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "tipo_documento" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "numero_documento" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(254) COLLATE "pg_catalog"."default" NOT NULL,
  "telefono_contacto" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamptz(6) NOT NULL,
  "updated_at" timestamptz(6) NOT NULL,
  "asunto_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_content_type";
CREATE TABLE "public"."django_content_type" (
  "id" int4 NOT NULL DEFAULT nextval('django_content_type_id_seq'::regclass),
  "app_label" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "model" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "public"."django_content_type" VALUES (1, 'auth', 'permission');
INSERT INTO "public"."django_content_type" VALUES (2, 'auth', 'group');
INSERT INTO "public"."django_content_type" VALUES (3, 'contenttypes', 'contenttype');
INSERT INTO "public"."django_content_type" VALUES (4, 'sessions', 'session');
INSERT INTO "public"."django_content_type" VALUES (5, 'contacto', 'contacto');
INSERT INTO "public"."django_content_type" VALUES (6, 'contacto', 'comentario');
INSERT INTO "public"."django_content_type" VALUES (7, 'contacto', 'asunto');
INSERT INTO "public"."django_content_type" VALUES (8, 'administrador', 'customuser');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_migrations";
CREATE TABLE "public"."django_migrations" (
  "id" int4 NOT NULL DEFAULT nextval('django_migrations_id_seq'::regclass),
  "app" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "applied" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO "public"."django_migrations" VALUES (1, 'contenttypes', '0001_initial', '2020-01-27 16:09:13.634059-05');
INSERT INTO "public"."django_migrations" VALUES (2, 'contenttypes', '0002_remove_content_type_name', '2020-01-27 16:09:13.647664-05');
INSERT INTO "public"."django_migrations" VALUES (3, 'auth', '0001_initial', '2020-01-27 16:09:13.685751-05');
INSERT INTO "public"."django_migrations" VALUES (4, 'auth', '0002_alter_permission_name_max_length', '2020-01-27 16:09:13.720099-05');
INSERT INTO "public"."django_migrations" VALUES (5, 'auth', '0003_alter_user_email_max_length', '2020-01-27 16:09:13.731171-05');
INSERT INTO "public"."django_migrations" VALUES (6, 'auth', '0004_alter_user_username_opts', '2020-01-27 16:09:13.740262-05');
INSERT INTO "public"."django_migrations" VALUES (7, 'auth', '0005_alter_user_last_login_null', '2020-01-27 16:09:13.753124-05');
INSERT INTO "public"."django_migrations" VALUES (8, 'auth', '0006_require_contenttypes_0002', '2020-01-27 16:09:13.755725-05');
INSERT INTO "public"."django_migrations" VALUES (9, 'auth', '0007_alter_validators_add_error_messages', '2020-01-27 16:09:13.766557-05');
INSERT INTO "public"."django_migrations" VALUES (10, 'auth', '0008_alter_user_username_max_length', '2020-01-27 16:09:13.775644-05');
INSERT INTO "public"."django_migrations" VALUES (11, 'auth', '0009_alter_user_last_name_max_length', '2020-01-27 16:09:13.791851-05');
INSERT INTO "public"."django_migrations" VALUES (12, 'auth', '0010_alter_group_name_max_length', '2020-01-27 16:09:13.804665-05');
INSERT INTO "public"."django_migrations" VALUES (13, 'auth', '0011_update_proxy_permissions', '2020-01-27 16:09:13.813511-05');
INSERT INTO "public"."django_migrations" VALUES (14, 'administrador', '0001_initial', '2020-01-27 16:09:13.855314-05');
INSERT INTO "public"."django_migrations" VALUES (15, 'contacto', '0001_initial', '2020-01-27 16:09:13.916739-05');
INSERT INTO "public"."django_migrations" VALUES (16, 'sessions', '0001_initial', '2020-01-27 16:09:13.936234-05');
INSERT INTO "public"."django_migrations" VALUES (17, 'contacto', '0002_auto_20200127_2117', '2020-01-27 16:17:41.197821-05');
INSERT INTO "public"."django_migrations" VALUES (18, 'contacto', '0003_auto_20200127_2127', '2020-01-27 16:27:42.582164-05');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_session";
CREATE TABLE "public"."django_session" (
  "session_key" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "session_data" text COLLATE "pg_catalog"."default" NOT NULL,
  "expire_date" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO "public"."django_session" VALUES ('3f7rudwukqrfyf5kf457zm84ioucbt7s', 'YWY2MjlhNWM4ZjAwZDYzYjgxYjczYTgyZWI0ZWEwZTVkMjQyNjFlNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YTc0MWExYzE5OWZiYzE2OTBiOWU1ZjM0ODE0OWJjY2MxY2JmZTNjIn0=', '2020-02-10 17:04:56.663153-05');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."administrador_customuser_groups_id_seq"
OWNED BY "public"."administrador_customuser_groups"."id";
SELECT setval('"public"."administrador_customuser_groups_id_seq"', 2, false);
ALTER SEQUENCE "public"."administrador_customuser_id_seq"
OWNED BY "public"."administrador_customuser"."id";
SELECT setval('"public"."administrador_customuser_id_seq"', 2, true);
ALTER SEQUENCE "public"."administrador_customuser_user_permissions_id_seq"
OWNED BY "public"."administrador_customuser_user_permissions"."id";
SELECT setval('"public"."administrador_customuser_user_permissions_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_group_id_seq"
OWNED BY "public"."auth_group"."id";
SELECT setval('"public"."auth_group_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_group_permissions_id_seq"
OWNED BY "public"."auth_group_permissions"."id";
SELECT setval('"public"."auth_group_permissions_id_seq"', 2, false);
ALTER SEQUENCE "public"."auth_permission_id_seq"
OWNED BY "public"."auth_permission"."id";
SELECT setval('"public"."auth_permission_id_seq"', 33, true);
ALTER SEQUENCE "public"."contacto_asunto_id_seq"
OWNED BY "public"."contacto_asunto"."id";
SELECT setval('"public"."contacto_asunto_id_seq"', 2, true);
ALTER SEQUENCE "public"."contacto_comentario_id_seq"
OWNED BY "public"."contacto_comentario"."id";
SELECT setval('"public"."contacto_comentario_id_seq"', 2, true);
ALTER SEQUENCE "public"."contacto_contacto_id_seq"
OWNED BY "public"."contacto_contacto"."id";
SELECT setval('"public"."contacto_contacto_id_seq"', 2, true);
ALTER SEQUENCE "public"."django_content_type_id_seq"
OWNED BY "public"."django_content_type"."id";
SELECT setval('"public"."django_content_type_id_seq"', 9, true);
ALTER SEQUENCE "public"."django_migrations_id_seq"
OWNED BY "public"."django_migrations"."id";
SELECT setval('"public"."django_migrations_id_seq"', 19, true);

-- ----------------------------
-- Indexes structure for table administrador_customuser
-- ----------------------------
CREATE INDEX "administrador_customuser_email_702d3726_like" ON "public"."administrador_customuser" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table administrador_customuser
-- ----------------------------
ALTER TABLE "public"."administrador_customuser" ADD CONSTRAINT "administrador_customuser_email_key" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table administrador_customuser
-- ----------------------------
ALTER TABLE "public"."administrador_customuser" ADD CONSTRAINT "administrador_customuser_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table administrador_customuser_groups
-- ----------------------------
CREATE INDEX "administrador_customuser_groups_customuser_id_b26e0109" ON "public"."administrador_customuser_groups" USING btree (
  "customuser_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "administrador_customuser_groups_group_id_bae3d983" ON "public"."administrador_customuser_groups" USING btree (
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table administrador_customuser_groups
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_groups" ADD CONSTRAINT "administrador_customuser_customuser_id_group_id_1617158b_uniq" UNIQUE ("customuser_id", "group_id");

-- ----------------------------
-- Primary Key structure for table administrador_customuser_groups
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_groups" ADD CONSTRAINT "administrador_customuser_groups_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table administrador_customuser_user_permissions
-- ----------------------------
CREATE INDEX "administrador_customuser_u_customuser_id_9c1a6c11" ON "public"."administrador_customuser_user_permissions" USING btree (
  "customuser_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "administrador_customuser_u_permission_id_8b054260" ON "public"."administrador_customuser_user_permissions" USING btree (
  "permission_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table administrador_customuser_user_permissions
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_user_permissions" ADD CONSTRAINT "administrador_customuser_customuser_id_permission_d6bfe4d9_uniq" UNIQUE ("customuser_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table administrador_customuser_user_permissions
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_user_permissions" ADD CONSTRAINT "administrador_customuser_user_permissions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group
-- ----------------------------
CREATE INDEX "auth_group_name_a6ea08ec_like" ON "public"."auth_group" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD CONSTRAINT "auth_group_name_key" UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD CONSTRAINT "auth_group_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id_b120cbf9" ON "public"."auth_group_permissions" USING btree (
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e" ON "public"."auth_group_permissions" USING btree (
  "permission_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_group_id_permission_id_0cd325b0_uniq" UNIQUE ("group_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id_2f476e4b" ON "public"."auth_permission" USING btree (
  "content_type_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_content_type_id_codename_01ab375a_uniq" UNIQUE ("content_type_id", "codename");

-- ----------------------------
-- Primary Key structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contacto_asunto
-- ----------------------------
ALTER TABLE "public"."contacto_asunto" ADD CONSTRAINT "contacto_asunto_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table contacto_comentario
-- ----------------------------
CREATE INDEX "contacto_comentario_contacto_id_b4c405f5" ON "public"."contacto_comentario" USING btree (
  "contacto_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table contacto_comentario
-- ----------------------------
ALTER TABLE "public"."contacto_comentario" ADD CONSTRAINT "contacto_comentario_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table contacto_contacto
-- ----------------------------
CREATE INDEX "contacto_contacto_asunto_id_4bd79e4c" ON "public"."contacto_contacto" USING btree (
  "asunto_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table contacto_contacto
-- ----------------------------
ALTER TABLE "public"."contacto_contacto" ADD CONSTRAINT "contacto_contacto_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD CONSTRAINT "django_content_type_app_label_model_76bd3d3b_uniq" UNIQUE ("app_label", "model");

-- ----------------------------
-- Primary Key structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD CONSTRAINT "django_content_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table django_migrations
-- ----------------------------
ALTER TABLE "public"."django_migrations" ADD CONSTRAINT "django_migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_expire_date_a5c62663" ON "public"."django_session" USING btree (
  "expire_date" "pg_catalog"."timestamptz_ops" ASC NULLS LAST
);
CREATE INDEX "django_session_session_key_c0390e0f_like" ON "public"."django_session" USING btree (
  "session_key" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table django_session
-- ----------------------------
ALTER TABLE "public"."django_session" ADD CONSTRAINT "django_session_pkey" PRIMARY KEY ("session_key");

-- ----------------------------
-- Foreign Keys structure for table administrador_customuser_groups
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_groups" ADD CONSTRAINT "administrador_custom_customuser_id_b26e0109_fk_administr" FOREIGN KEY ("customuser_id") REFERENCES "public"."administrador_customuser" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."administrador_customuser_groups" ADD CONSTRAINT "administrador_custom_group_id_bae3d983_fk_auth_grou" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table administrador_customuser_user_permissions
-- ----------------------------
ALTER TABLE "public"."administrador_customuser_user_permissions" ADD CONSTRAINT "administrador_custom_customuser_id_9c1a6c11_fk_administr" FOREIGN KEY ("customuser_id") REFERENCES "public"."administrador_customuser" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."administrador_customuser_user_permissions" ADD CONSTRAINT "administrador_custom_permission_id_8b054260_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissio_permission_id_84c5c92e_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_group_id_b120cbf9_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD CONSTRAINT "auth_permission_content_type_id_2f476e4b_fk_django_co" FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table contacto_comentario
-- ----------------------------
ALTER TABLE "public"."contacto_comentario" ADD CONSTRAINT "contacto_comentario_contacto_id_b4c405f5_fk_contacto_" FOREIGN KEY ("contacto_id") REFERENCES "public"."contacto_contacto" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Keys structure for table contacto_contacto
-- ----------------------------
ALTER TABLE "public"."contacto_contacto" ADD CONSTRAINT "contacto_contacto_asunto_id_4bd79e4c_fk_contacto_asunto_id" FOREIGN KEY ("asunto_id") REFERENCES "public"."contacto_asunto" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
