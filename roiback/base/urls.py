from django.conf.urls import url
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

from . import views

app_name = 'base'

urlpatterns = [
    # redirecciona al template de inicio de sesion
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    # redirecciona al template de inicio de sesion una vez cierre sesion
    path('logout/', LogoutView.as_view(template_name='login.html'), name='logout'),

]