from django.shortcuts import render

# Create your views here.
def index(request):
    """Vista para mostrar el template base"""
    return render(request, "base.html")

def login(request):
    """Vista para mostrar el template del login"""
    return render(request, 'login.html')