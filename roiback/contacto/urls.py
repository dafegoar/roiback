from django.conf.urls import url

from . import views

app_name = 'contacto'

urlpatterns = [
    url('index/', views.index, name='index'),
    url('edit-peticion/(?P<id_peticion>[0-9]+)/$', views.editar_peticion, name='editar_peticion'),
    url('delete-peticion/(?P<id_peticion>[0-9]+)/$', views.eliminar_peticion, name='eliminar_peticion'),
]