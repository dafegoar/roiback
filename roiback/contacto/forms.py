from django import forms
from .models import *


class DateInput(forms.DateInput):
    input_type = 'date'


class ContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = ['nombre', 'apellido', 'fecha_nacimiento',
                  'genero', 'tipo_documento', 'numero_documento',
                  'email', 'telefono_contacto', 'asunto' ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento': DateInput(),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'tipo_documento': forms.Select(attrs={'class': 'form-control'}),
            'numero_documento': forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'telefono_contacto': forms.NumberInput(attrs={'class': 'form-control'}),
            'asunto': forms.Select(attrs={'class': 'form-control'})
        }
        labels = {
            'nombre': ('Nombres'),
            'apellido': ('Apellidos'),
            'fecha_nacimiento': ('Fecha Nacimiento'),
            'genero': ('Genero'),
            'tipo_documento': ('Tipo de Documento'),
            'numero_documento': ('Numero de Documento'),
            'email': ('Email'),
            'telefono_contacto': ('Telefono de Contacto'),
            'asunto':('Asunto')
        }

class AsuntoForm(forms.ModelForm):
    class Meta:
        model = Asunto
        fields = ['asunto']
        widgets = {
            'asunto': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'asunto': ('Asunto'),
        }


class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['comentario']
        widgets = {
            'comentario': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'comentario': ('Comentario'),
        }