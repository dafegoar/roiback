from django.shortcuts import render, redirect
from contacto.forms import *
from contacto.models import *


def index(request):
    """Vista que carga el formulario de contacto"""
    if request.method == 'POST':
        form = ContactoForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            contacto = form.save()
            return redirect('/index/')
        else:
            print("Formulario no valido")
            return render(request, "formulario.html", {'form': form})
    else:
        form = ContactoForm()
        return render(request, "formulario.html", {'form': form,})


def editar_peticion(request, id_peticion):
    """Vista que permite editar los valores de un registro"""

    peticion = Contacto.objects.get(pk=id_peticion)
    peticiones = Contacto.objects.all()

    if request.method == 'GET':
        # Get se usa para obtener datos del formulario
        form = ContactoForm(instance=peticion)
    else:
        # Aqui guardamos la peticion
        form = ContactoForm(request.POST, instance=peticion)
        if form.is_valid():
            form.save()
            return render(request, 'home.html', {'peticiones': peticiones})
    return render(request, 'editar_peticion.html', {'form': form})


def eliminar_peticion(request, id_peticion):
    """Vista que permite eliminar un registro de peticion"""
    peticion = Contacto.objects.get(pk=id_peticion)
    peticion.delete()
    peticiones = Contacto.objects.all()
    return render(request, 'home.html', {'peticiones': peticiones})



