from django.db import models
from contacto.choices import GENERO_CHOICES, TIPO_DOCUMENTO_CHOICES


class Asunto(models.Model):
    asunto = models.CharField(max_length=100, null=False)
    def __str__(self):
        return self.asunto


class Contacto(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    apellido = models.CharField(max_length=100, null=False)
    fecha_nacimiento = models.DateField()
    genero = models.CharField(max_length=100,choices=GENERO_CHOICES, null=False)
    tipo_documento = models.CharField(max_length=100,choices=TIPO_DOCUMENTO_CHOICES, null=False)
    numero_documento = models.CharField(max_length=100, null=False)
    email = models.EmailField(max_length = 254)
    telefono_contacto = models.CharField(max_length=100, null=False)
    asunto = models.ForeignKey(Asunto, on_delete=models.CASCADE, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Comentario(models.Model):
    comentario = models.CharField(max_length=200, null=True)
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, null=False, blank=False)