GENERO_CHOICES = (
    ('MASCULINO', 'Masculino'),
    ('FEMENINO', 'Femenino'),
)
TIPO_DOCUMENTO_CHOICES = (
        ('CC','Cedula de Ciudadania'),
        ('TI', 'Tarjeta de Identidad'),
        ('CE', 'Cedula de Extranjeria'),
        ('TE', 'Tarjeta de Extranjeria'),
        ('PAS', 'Pasaporte'),
    )