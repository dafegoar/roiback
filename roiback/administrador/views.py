from django.shortcuts import render, redirect
from contacto.models import Contacto, Comentario
from contacto.forms import AsuntoForm, ComentarioForm
from administrador.forms import CustomUserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/login/')
def home(request):
    """Vista que permite visualizar el listado de peticiones"""
    peticiones = Contacto.objects.all()
    return render(request, 'home.html', {'peticiones': peticiones})


@login_required(login_url='/login/')
def crear_administrador(request):
    """Vista que permite crear un nuevo usuario administrador"""
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            usuario = form.save(commit=False)
            usuario.save()
            return redirect('/administrador/home/')
        else:
            return render(request, 'crear_usuario.html', {'form': form})
    else:
        form = CustomUserCreationForm()
        return render(request, 'crear_usuario.html', {'form': form})


@login_required(login_url='/login/')
def crear_asunto(request):
    """Vista que permite crear un nuevo asunto"""
    if request.method == 'POST':
        form = AsuntoForm(request.POST)
        if form.is_valid():
            asunto = form.save(commit=False)
            asunto.save()
            return redirect('/administrador/home/')
        else:
            return render(request, 'crear_asunto.html', {'form': form})
    else:
        form = AsuntoForm()
        return render(request, 'crear_asunto.html', {'form': form})


@login_required(login_url='/login/')
def crear_comentario(request, id_peticion):
    """Vista que permite crear un nuevo comentario"""
    contacto = Contacto.objects.get(pk=id_peticion)
    if request.method == 'POST':
        form = ComentarioForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            comentario = Comentario (
                comentario = data['comentario'],
                contacto = contacto
            )
            comentario.save()
            return redirect('/administrador/home/')
        else:
            return render(request, 'crear_comentario.html', {'form': form})
    else:
        form = ComentarioForm()
        return render(request, 'crear_comentario.html', {'form': form})