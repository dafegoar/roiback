from django.conf.urls import url

from . import views

app_name = 'administrador'

urlpatterns = [
    url(r'^home', views.home, name='home'),
    url(r'^nuevo_administrador', views.crear_administrador, name='crear_administrador'),
    url(r'^nuevo_asunto', views.crear_asunto, name='crear_asunto'),
    url('crear-comentario/(?P<id_peticion>[0-9]+)/$', views.crear_comentario, name='crear_comentario'),
]